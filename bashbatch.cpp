#include "bashbatch.h"

#include "common.h"

BashBatch::BashBatch(QMap<QString, QString> &env)
    : m_env(env)
{
}

int BashBatch::exec(QString cmd, QString workingDirectory)
{
    //qDebug() << m_env;
    //qDebug() << m_env["path"];
    qDebug() << cmd << workingDirectory;
    QDateTime dt = QDateTime::currentDateTime();
    QTemporaryFile tempFile(m_env["temp"] + "/" + dt.toString("yyyy-MM-dd-hh-mm-ss") + "-XXXXXX.sh");
    if(tempFile.open())
    {
        qDebug() << tempFile.fileName();
        //cmd = QString("cd '%1'\n").arg(workingDirectory) + cmd;
        tempFile.write(cmd.toLocal8Bit());
        tempFile.close();
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        env.insert("HOME", np(m_env["temp"]));
        env.insert("PATH", np(m_env["dir"] + "/cmd") + ";" + m_env["path"]);
        QProcess proc;
        proc.setProgram("busybox.exe");
        proc.setProcessEnvironment(env);
        proc.setWorkingDirectory(np(workingDirectory));
        proc.setArguments(QStringList() << "bash" << "-c" << tempFile.fileName());
        //int exitCode = proc.execute("busybox.exe", QStringList() << "bash" << "-c" << tempFile.fileName());
        //qDebug() << "exitCode=" << exitCode;
        //return exitCode;
#if 0x1
#if 0x0
        bool finished = false;
        QObject::connect(&proc, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
                         [&finished](int exitCode, QProcess::ExitStatus exitStatus)
        {
            finished = true;
        });
        proc.start();
        //while(proc.state()==QProcess::Starting || proc.state()==QProcess::Running)
        while(!finished)
        {
            qApp->processEvents();
        }
#endif
        proc.waitForFinished(100000000);
        QString output = QString::fromLocal8Bit(proc.readAll());
        qDebug().noquote() << output;
        int exitCode = proc.exitCode();
        qDebug() << "exitCode=" << exitCode;
        return exitCode;
#endif
    }
    return -1;
}
