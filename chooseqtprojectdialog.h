#ifndef CHOOSEQTPROJECTDIALOG_H
#define CHOOSEQTPROJECTDIALOG_H

//#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QtWidgets>

namespace Ui
{
class ChooseQtProjectDialog;
}

class ChooseQtProjectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChooseQtProjectDialog(QStringList proList, QWidget *parent = nullptr);
    ~ChooseQtProjectDialog();
    QString proFile() const;

private:
    QStringList m_proList;
    QString m_proFile;

private slots:
    void on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_listWidget_currentRowChanged(int currentRow);

    void on_listWidget_itemSelectionChanged();

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::ChooseQtProjectDialog *ui;
};

#endif // CHOOSEQTPROJECTDIALOG_H
