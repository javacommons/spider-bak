#! bash -uvx
#./busybox_tilde64.exe --install ./cmd
#rm cmd/busybox.exe
#cp busybox_tilde64.exe cmd/busybox.exe
#rm -rf cmd/ar.exe cmd/strings.exe cmd/unzip.exe
rm -rf busybox.tmp
mkdir -p busybox.tmp
./busybox_tilde64.exe --install ./busybox.tmp
cd busybox.tmp
rm -rf ar.exe strings.exe unzip.exe
zip -r ../busybox-tilde64.zip *
