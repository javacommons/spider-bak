QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
#CONFIG += console

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#DEFINES += QUAZIP_STATIC
#LIBS += -lquazip_static -lz
#LIBS += -lssl -lcrypto -lws2_32 -lcrypt32

LIBS = -larchive -lz -lbz2 -llzma -liconv -lbcrypt -lexpat -lb2 -llz4

SOURCES += \
    archiver.cpp \
    bashbatch.cpp \
    binarysettings.cpp \
    chooseqtprojectdialog.cpp \
    cmdprocess.cpp \
    envvardialog.cpp \
    jsonserializer.cpp \
    main.cpp \
    msys2dialog.cpp \
    programdb.cpp \
    projectchecker.cpp \
    qsettings_binary.cpp \
    qsettings_json.cpp \
    repodialog.cpp \
    scoopsoftwaredialog.cpp \
    scoopsoftwarelist.cpp \
    variantserializer.cpp \
    widget.cpp \
    zip/zip.c

HEADERS += \
    archiver.h \
    bashbatch.h \
    binarysettings.h \
    chooseqtprojectdialog.h \
    cmdprocess.h \
    common.h \
    envvardialog.h \
    jsonserializer.h \
    msys2dialog.h \
    programdb.h \
    projectchecker.h \
    qsettings_binary.h \
    qsettings_json.h \
    repodialog.h \
    scoopsoftwaredialog.h \
    scoopsoftwarelist.h \
    variantserializer.h \
    widget.h \
    zip/miniz.h \
    zip/zip.h

FORMS += \
    chooseqtprojectdialog.ui \
    envvardialog.ui \
    msys2dialog.ui \
    repodialog.ui \
    scoopsoftwaredialog.ui \
    widget.ui

INCLUDEPATH += $$(HOME)/include

DESTDIR = $$PWD

TARGET = $${TARGET}-$${QMAKE_HOST.arch}

#message($$QMAKE_QMAKE)
contains(QMAKE_QMAKE, .*\-static\/.*) {
    message( "[STATIC BUILD]" )
    DEFINES += QT_STATIC_BUILD
    TARGET = $${TARGET}-static
} else {
    message( "[SHARED BUILD]" )
}

# https://www.flaticon.com/free-icon/spider-web_2250473?term=spider&page=1&position=2&page=1&position=2&related_id=2250473&origin=tag
RC_ICONS = spider-web.ico

RESOURCES += \
    spider.qrc

include(SingleApplication/singleapplication.pri)
DEFINES += QAPPLICATION_CLASS=QApplication
