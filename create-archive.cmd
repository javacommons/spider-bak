set cwd=%cd%

set product=WindowsTerminal
set version=1.12.10393.0
rm %cwd%\%product%-%version%.zip
cd %cwd%\%product%-%version%
zip -r %cwd%\%product%-%version%.zip *
cd %cwd%

set product=sqlitestudio
set version=3.3.3
rm %cwd%\%product%-%version%.zip
cd %cwd%\%product%-%version%
zip -r %cwd%\%product%-%version%.zip *
cd %cwd%

set product=FolderSizePortable
set version=4.9.5.0
rm %cwd%\%product%-%version%.zip
cd %cwd%\%product%-%version%
zip -r %cwd%\%product%-%version%.zip *
cd %cwd%
