#include "chooseqtprojectdialog.h"
#include "ui_chooseqtprojectdialog.h"

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

ChooseQtProjectDialog::ChooseQtProjectDialog(QStringList proList, QWidget *parent) :
    QDialog(parent),
    m_proList(proList),
    ui(new Ui::ChooseQtProjectDialog)
{
    ui->setupUi(this);
    ((QPushButton *)ui->buttonBox->buttons().first())->setEnabled(false);
    ui->listWidget->blockSignals(true);
    foreach(QString proFile, proList)
    {
        ui->listWidget->addItem(QFileInfo(proFile).fileName());
        ui->listWidget->item(ui->listWidget->count()-1)->setData(Qt::UserRole, proFile);
    }
    ui->listWidget->blockSignals(false);
}

ChooseQtProjectDialog::~ChooseQtProjectDialog()
{
    delete ui;
}

QString ChooseQtProjectDialog::proFile() const
{
    return m_proFile;
}

void ChooseQtProjectDialog::on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
}

void ChooseQtProjectDialog::on_listWidget_currentRowChanged(int currentRow)
{
}


void ChooseQtProjectDialog::on_listWidget_itemSelectionChanged()
{
    //qDebug() << "currentRow=" << currentRow;
    //QListWidgetItem *current = ui->listWidget->item(currentRow);
    QListWidgetItem *current = ui->listWidget->selectedItems().at(0);
    m_proFile = current->data(Qt::UserRole).toString();
    ((QPushButton *)ui->buttonBox->buttons().first())->setEnabled(true);
}


void ChooseQtProjectDialog::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    m_proFile = item->data(Qt::UserRole).toString();
    this->accept();
}

