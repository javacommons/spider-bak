@echo off
where runemacs.exe > nul 2>&1
if %ERRORLEVEL% neq 0 (
  call scoop bucket add extras
  call scoop install emacs
)
runemacs %*
rem emacs -nw %*
