#! bash -uvx
#! bash -uvx
set -e
rm -rf build-spider.tmp
astyle --style=allman --recursive  *.java,*.c,*.cpp,*.cxx,*.h,*.hpp,*.hxx
sleep 3
ts=`date "+%Y.%m.%d.%H.%M.%S"`
echo $ts > spider.version.txt
UNAME=`uname`
if [ "$UNAME" = "Windows_NT" ]; then
  mingwx.cmd mk.mgw spider
else
  mk.mgw spider
fi
rm -rf spider-*.7z
#cp -rp rapidee.* cmd/
7z a -t7z spider-$ts.7z spider-x86_64-static.exe scoop-sw-list.ini cmd temp -x!temp/*
sha256sum spider-$ts.7z
sum1=`sha256sum spider-$ts.7z | awk '{print $1}'`
echo $sum1
cat << EOS > spider.json
{
    "version": "$ts",
    "description": "",
    "homepage": "",
    "license": "MIT",
    "depends": [
        "main/7zip",
        "main/git",
        "main/curl",
        "main/unzip",
        "main/zip",
        "main/elvish",
        "main/file",
        "main/astyle",
        "main/nyagos"
    ],
    url: [
        "https://gitlab.com/javacommons/spider/-/raw/main/spider-$ts.7z"
    ],
    "hash": [
        "$sum1"
    ],
    "bin": [
        [
            "spider-x86_64-static.exe",
            "spider",
            "--dumy"
        ]
    ],
    "shortcuts": [
        [
            "spider-x86_64-static.exe",
            "Spider",
            "--dummy"
        ]
    ],
    "persist": "temp"
}
EOS
git add .
#git commit -m.
git commit -m"Spider v$ts"
git push
