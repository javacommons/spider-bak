#include "widget.h"
#include "ui_widget.h"

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include "common.h"

#include "msys2dialog.h"
#include "repodialog.h"
#include "envvardialog.h"
#include "scoopsoftwaredialog.h"

#include "cmdprocess.h"
#include "projectchecker.h"
#include "scoopsoftwarelist.h"
#include "programdb.h"

#include "qsettings_json.h"
#include "variantserializer.h"

static void settingsTest(BinarySettings &settings, bool write)
{
    if(write)
    {
        QVariantMap map;
        map["a"] = 1;
        map["b"] = "ハロー©";
        map["c"] = QDateTime::currentDateTime();
        QVariantList list;
        list.append(123.456);
        list.append("ハロー©");
        settings.beginGroup("group1");
        settings.setBinary("map", map);
        settings.setValue("list", list);
        settings.endGroup();
    }
    {
        settings.beginGroup("group1");
        qDebug() << settings.binary("map");
        qDebug() << settings.value("list");
        settings.endGroup();
    }
}

Widget::Widget(const QMap<QString, QString> env, QWidget *parent)
    : QWidget(parent),
      m_env(env),
      ui(new Ui::Widget)
{
    ui->setupUi(this);
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)),
            SLOT(showContextMenuForListWidget1(const QPoint &)));
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)),
            SLOT(showContextMenuForListWidget2(const QPoint &)));
    //this->ui->listWidget->setDragDropMode(QAbstractItemView::InternalMove); // Drag & Drop Item Sort
#if 0x0
    m_env["dir"] = qApp->applicationDirPath();
    m_env["temp"] = m_env["dir"] + "/temp";
    m_env["prof"] = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    m_env["docs"] = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    m_env["msys2"] = m_env["prof"] + "/.software/msys2";
    //m_env["wt"] = m_env["dir"] + "/wt-1.12.10393.0";
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    //m_env["path"] = np(m_env["wt"]) + ";" + env.value("PATH");
    m_env["path"] = env.value("PATH");
#endif
    QFile version(":/spider.version.txt");
    if(version.open(QIODevice::ReadOnly))
    {
        QByteArray versionBytes = version.readAll().trimmed();
        qDebug() << "version=" << versionBytes;
        this->setWindowTitle(QString("Spider v%1").arg(QString::fromLatin1(versionBytes)));
    }
    this->reloadNameList();
    connect(&m_reload_timer, &QTimer::timeout, [this]()
    {
        this->reloadNameList();
    });
    m_reload_timer.start(500);
#if 0x1
    MySettings settings;
    settingsTest(settings, true);
#else
    BinarySettings settings;
    settingsTest(settings, true);
#endif
}

Widget::~Widget()
{
    delete ui;
}

void Widget::reloadNameList(bool force)
{
    MySettings settings;
    // ファイルフィルタ
    QDir::Filters filters = QDir::Dirs;
    // 対象フラグ
    QDirIterator::IteratorFlags flags = QDirIterator::NoIteratorFlags;

    QDirIterator it0(m_env["docs"] + "/.repo", filters, flags);
    //QStringList realRepoList;
    QMap<QString, bool> realRepoList;
    while (it0.hasNext())
    {
        QString dir = it0.next();
        if(dir.endsWith("/.") || dir.endsWith("/..")) continue;
        ProjectChecker ck(dir);
        if(!ck.isVisible()) continue;
        if(ck.isGitDir())
        {
            //realRepoList.append("*" + QFileInfo(dir).fileName());
            realRepoList.insert(QFileInfo(dir).fileName(), true);
        }
        else
        {
            //realRepoList.append(QFileInfo(dir).fileName());
            realRepoList.insert(QFileInfo(dir).fileName(), false);
        }
    }
    //qDebug() << realRepoList;
    updateListWidgetContent(ui->listWidget, &realRepoList, settings.value(settings.SELECTED_REPO_NAME).toString(), force);

    QDirIterator it2(m_env["msys2"], filters, flags);
    //QStringList realMsys2List;
    QMap<QString, bool> realMsys2List;
    while (it2.hasNext())
    {
        QString dir = it2.next();
        if(dir.endsWith("/.") || dir.endsWith("/..")) continue;
        if(!QFile(dir + "/msys2_shell.cmd").exists()) continue;
        //realMsys2List.append(QFileInfo(dir).fileName());
        realMsys2List.insert(QFileInfo(dir).fileName(), false);
    }
    //qDebug() << realMsys2List;
    QStringList currMsys2List;
    updateListWidgetContent(ui->listWidget_2, &realMsys2List, settings.value(settings.SELECTED_MSYS2_NAME).toString(), force);
}

QString Widget::selectedRepoName()
{
    MySettings settings;
    QString repo = settings.value(settings.SELECTED_REPO_NAME).toString();
    QString repoDir = m_env["docs"] + "/.repo/" + repo;
    qDebug() << repoDir << QDir(repoDir).exists();
    if(!QDir(repoDir).exists())
    {
        settings.setValue(settings.SELECTED_REPO_NAME, "");
        return "";
    }
    return repo;
}

QString Widget::selectedMsys2Name()
{
    MySettings settings;
    QString msys2 = settings.value(settings.SELECTED_MSYS2_NAME).toString();
    QString msys2Dir = m_env["prof"] + "/.software/msys2/" + msys2;
    if(!QDir(msys2Dir).exists())
    {
        settings.setValue(settings.SELECTED_MSYS2_NAME, "");
        return "";
    }
    return msys2;
}

void Widget::run_process(bool need_msys2, QString forcedMsys2, ProcCallback callback)
{
    auto uhomeName = this->selectedRepoName();
    QString uhomeDir = m_env["docs"] + "/.repo/" + uhomeName;
    if(uhomeName.isEmpty())
    {
        uhomeDir = m_env["docs"] + "/.repo";
        QDir docsDir = m_env["docs"];
        docsDir.mkpath(".repo");
    }
    QString cmd1 = m_env["dir"] + "/cmd";
    QString cmd2 = uhomeDir + "/cmd";
    QString pathAdded = np(cmd1) + ";" + np(cmd2);
    auto msys2Name = (forcedMsys2.isEmpty()) ? this->selectedMsys2Name() : forcedMsys2;
    if(need_msys2)
    {
        if(msys2Name.isEmpty())
        {
            if(ui->listWidget_2->count()>0)
            {
                QMessageBox::StandardButton reply;
                reply = QMessageBox::question(this, "確認", "MSYS2を選択せずに起動しますか?",
                                              QMessageBox::Yes|QMessageBox::No);
                if (reply == QMessageBox::Yes)
                {
                    qDebug() << "Yes was clicked";
                    msys2Name = "(none)";
                }
                else
                {
                    qDebug() << "Yes was *not* clicked";
                    QMessageBox::information(this, "確認", "MSYS2を選択してください");
                    return;
                }
            }
        }
        else
        {
            pathAdded += ";";
            pathAdded += np(m_env["msys2"] + "/" + msys2Name);
        }
    }
    qDebug() << "pathAdded=" << pathAdded;
    QProcess proc;
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    SpiderSettings settings(m_env, uhomeName);
    settings.settings().beginGroup("environmentVariable");
    auto keys = settings.settings().childKeys();
    for(int i=0; i<keys.size(); i++)
    {
        env.insert(keys[i], settings.settings().value(keys[i]).toString());
    }
    settings.settings().endGroup();
    env.insert("HOME", np(uhomeDir));
    env.insert("PATH", pathAdded + ";" + m_env["path"]);
    env.insert("REPO", uhomeName);
    env.insert("MSYS2", msys2Name);
    proc.setProcessEnvironment(env);
    proc.setWorkingDirectory(np(uhomeDir));
    callback(proc, ProcStage::SETUP, true);
    if(proc.startDetached())
    {
        callback(proc, ProcStage::FINISH, true);
    }
    else
    {
        callback(proc, ProcStage::FINISH, false);
    }
}

void Widget::clone_repo()
{
    RepoDialog dlg;
    if(!dlg.exec()) return;
    QString repoUrl = dlg.url();
    QString buffer;
    QTextStream strm(&buffer);
    strm << QString("#! bash -uvx") << Qt::endl;
    strm << QString("set -e") << Qt::endl;
    strm << QString("pwd") << Qt::endl;
    strm << QString("cd %1").arg(m_env["docs"]) << Qt::endl;
    strm << QString("mkdir -p .repo") << Qt::endl;
    strm << QString("cd .repo") << Qt::endl;
    strm << QString("git clone --recursive %1").arg(repoUrl) << Qt::endl;
    strm << Qt::flush;
    QString cmdLines = *strm.string();
    CmdProcess *proc = new CmdProcess(m_env, QString("リポジトリをクローン"), cmdLines, ".sh");
    proc->run();
}

void Widget::remove_repo(QString repo)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "確認", QString("%1を削除しますか?").arg(repo),
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply != QMessageBox::Yes)
    {
        return;
    }
    QString buffer;
    QTextStream strm(&buffer);
    strm << QString("#! bash -uvx") << Qt::endl;
    strm << QString("set -e") << Qt::endl;
    strm << QString("pwd") << Qt::endl;
    strm << QString("cd %1").arg(m_env["docs"]) << Qt::endl;
    strm << QString("cd .repo") << Qt::endl;
    strm << QString("mv %1 %1.deleting").arg(repo) << Qt::endl;
    strm << QString("rm -rvf %1.deleting").arg(repo) << Qt::endl;
    strm << Qt::flush;
    QString cmdLines = *strm.string();
    CmdProcess *proc = new CmdProcess(m_env, QString("%1 を削除").arg(repo), cmdLines, ".sh");
    proc->run();
}

void Widget::refresh_repo(QString repo)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "確認", QString("%1を削除してクローンしなおしますか?").arg(repo),
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply != QMessageBox::Yes)
    {
        return;
    }
    QString buffer;
    QTextStream strm(&buffer);
    strm << QString("#! bash -uvx") << Qt::endl;
    strm << QString("set -e") << Qt::endl;
    strm << QString("pwd") << Qt::endl;
    strm << QString("cd %1/.repo/%2").arg(m_env["docs"]).arg(repo) << Qt::endl;
    strm << QString("url=`git config --get remote.origin.url`") << Qt::endl;
    strm << QString("cd %1/.repo").arg(m_env["docs"]) << Qt::endl;
    strm << QString("mv %1 %1.deleting").arg(repo) << Qt::endl;
    strm << QString("rm -rvf %1.deleting").arg(repo) << Qt::endl;
    strm << QString("git clone --recursive $url %1").arg(repo) << Qt::endl;
    strm << Qt::flush;
    QString cmdLines = *strm.string();
    CmdProcess *proc = new CmdProcess(m_env, QString("%1 を再取得").arg(repo), cmdLines, ".sh");
    proc->run();
}

void Widget::develop_with_qtcreator(QString proFile)
{
    this->run_process(true, "", [this, proFile](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            proc.setProgram(R"(cmd.exe)");
            proc.setArguments(QStringList() << "/c" << "mingw64x.cmd" << "start" << "qtcreator" << proFile);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                QMessageBox::information(this, "確認", QString("QtCreatorを起動しました(%1)").arg(QFileInfo(proFile).fileName()));
            }
            else
            {
                QMessageBox::information(this, "確認", QString("QtCreatorの起動が失敗しました(%1)").arg(QFileInfo(proFile).fileName()));
            }
        }
    });
}

void Widget::develop_with_android_studio(QString repoDir)
{
    QFileInfo android_studio = ProgramDB().android_studio();
    if(!android_studio.exists())
    {
        QMessageBox::information(this, "確認", "android-studioがインストールされていません");
        return;
    }
    this->run_process(true, "", [this, repoDir, android_studio](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            proc.setProgram(android_studio.absoluteFilePath());
            proc.setArguments(QStringList() << repoDir);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                QMessageBox::information(this, "確認", "Android Studioを起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "Android Studioの起動が失敗しました");
            }
        }
    });
}

void Widget::develop_with_idea(QString repoDir)
{
    QFileInfo idea = ProgramDB().idea();
    if(!idea.exists())
    {
        QMessageBox::information(this, "確認", "ideaがインストールされていません");
        return;
    }
    this->run_process(true, "", [this, repoDir, idea](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            proc.setProgram(idea.absoluteFilePath());
            proc.setArguments(QStringList() << repoDir);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                QMessageBox::information(this, "確認", "ideaを起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "ideaの起動が失敗しました");
            }
        }
    });
}

void Widget::open_emacs(QString repoDir)
{
    QFileInfo emacs = ProgramDB().emacs();
    if(!emacs.exists())
    {
        QMessageBox::information(this, "確認", "emacsがインストールされていません");
        return;
    }
    this->run_process(true, "", [this, repoDir, emacs](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            proc.setProgram(emacs.absoluteFilePath());
            proc.setArguments(QStringList() << repoDir);
            qDebug() << proc.arguments();
            proc.setWorkingDirectory(repoDir);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                //QMessageBox::information(this, "確認", "emacsを起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "emacsの起動が失敗しました");
            }
        }
    });
}

void Widget::open_vscode(QString repoDir)
{
    QFileInfo vscode = ProgramDB().vscode();
    if(!vscode.exists())
    {
        QMessageBox::information(this, "確認", "vscodeがインストールされていません");
        return;
    }
    this->run_process(true, "", [this, repoDir, vscode](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            proc.setProgram(vscode.absoluteFilePath());
            proc.setArguments(QStringList() << repoDir);
            qDebug() << proc.arguments();
            proc.setWorkingDirectory(repoDir);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                //QMessageBox::information(this, "確認", "vscodeを起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "vscodeの起動が失敗しました");
            }
        }
    });
}

void Widget::install_or_update_flutter()
{
    QString buffer;
    QTextStream strm(&buffer);
    strm << QString("busybox pwd") << Qt::endl;
    strm << QString("cd /d %1").arg(np(m_env["prof"])) << Qt::endl;
    strm << QString("if not exist .software mkdir .software") << Qt::endl;
    strm << QString("cd .software") << Qt::endl;
    strm << QString("if not exist flutter (") << Qt::endl;
    strm << QString("  git clone https://github.com/flutter/flutter.git -b stable") << Qt::endl;
    strm << QString("  envset.exe prepend PATH \"%1\"").arg(np(m_env["prof"] + "/.software/flutter/bin")) << Qt::endl;
    strm << QString(") else (") << Qt::endl;
    strm << QString("  call \"%1\" upgrade").arg(np(m_env["prof"] + "/.software/flutter/bin/flutter.bat")) << Qt::endl;
    strm << QString(")") << Qt::endl;
    strm << QString("call \"%1\" doctor").arg(np(m_env["prof"] + "/.software/flutter/bin/flutter.bat")) << Qt::endl;
    strm << Qt::flush;
    QString cmdLines = *strm.string();
    CmdProcess *proc = new CmdProcess(m_env, QString("flutter をインストール/アップデート"), cmdLines, ".cmd");
    proc->run();
}

void Widget::install_msys2()
{
    Msys2Dialog dlg;
    if(!dlg.exec()) return;
    QString msys2Name = dlg.name();
    QString archive = "msys2-base-x86_64-20220319.tar.xz";
    QString archive_file = np(QString("%1/%2").arg(m_env["temp"]).arg(archive));
    QString archive_url = QString("https://gitlab.com/javacommons/widget01/-/raw/main/%1").arg(archive);
    QString buffer;
    QTextStream strm(&buffer);
    strm << QString("busybox pwd") << Qt::endl;
    strm << QString("cd /d %1").arg(np(m_env["prof"])) << Qt::endl;
    strm << QString("if not exist %1 busybox wget -O %1 %2").arg(archive_file).arg(archive_url) << Qt::endl;
    strm << QString("if not exist .software\\msys2 mkdir .software\\msys2") << Qt::endl;
    strm << QString("cd .software\\msys2") << Qt::endl;
    strm << QString("if exist %1 rmdir /s /q %1").arg(msys2Name) << Qt::endl;
    strm << QString("mkdir %1").arg(msys2Name) << Qt::endl;
    strm << QString("busybox tar xvf %2 -C %1 --strip-components 1").arg(msys2Name).arg(archive_file) << Qt::endl;
    strm << QString("call %1\\msys2_shell.cmd -msys2 -defterm -here -no-start -c %2").arg(msys2Name).arg(QString("echo \"Installation Complete (%1)...\"").arg(msys2Name)) << Qt::endl;
    strm << QString("cd %1\\etc").arg(msys2Name) << Qt::endl;
    strm << QString("if not exist bash.bashrc.orig copy bash.bashrc bash.bashrc.orig") << Qt::endl;
    strm << QString("busybox sed -e \"s/^  export PS1=.*$/  export PS1='(%1@$MSYSTEM) \\\\w \\$ '/g\" bash.bashrc.orig > bash.bashrc").arg(msys2Name) << Qt::endl;
    strm << Qt::flush;
    QString cmdLines = *strm.string();
    CmdProcess *proc = new CmdProcess(m_env, QString("%1 をインストール").arg(msys2Name), cmdLines, ".cmd");
    proc->run();
}

void Widget::remove_msys2(QString name)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "確認", QString("%1を削除しますか?").arg(name),
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply != QMessageBox::Yes)
    {
        return;
    }
    qDebug() << "(1)";
#if 0x0
    MySettings settings;
    settings.setValue(settings.SELECTED_MSYS2_NAME, settings.value(settings.PREVIOUS_MSYS2_NAME));
#endif
    QString buffer;
    QTextStream strm(&buffer);
    strm << QString("busybox pwd") << Qt::endl;
    strm << QString("cd /d %1").arg(np(m_env["msys2"])) << Qt::endl;
    strm << QString("move %1 %1.deleting").arg(name) << Qt::endl;
    strm << QString("rmdir /s /q %1.deleting").arg(name) << Qt::endl;
    strm << Qt::flush;
    QString cmdLines = *strm.string();
    qDebug() << "(2)";
    CmdProcess *proc = new CmdProcess(m_env, QString("%1 を削除").arg(name), cmdLines, ".cmd");
    proc->run();
    qDebug() << "(end)";
}

void Widget::open_cmd(QString repo)
{
    auto wt = m_env["wt"]; //ProgramDB().wt().absoluteFilePath();
    auto uhomeName = this->selectedRepoName();
    auto msys2Name = this->selectedMsys2Name();
    QString repoDir = m_env["docs"] + "/.repo/" + repo;
    this->run_process(true, "", [this, wt, uhomeName, msys2Name, repoDir](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            if(wt.endsWith("/WindowsApps/wt.exe"))
            {
                proc.setProgram(wt);
                proc.setArguments(QStringList() << R"(nt)" << "--title" << QString("(Nyagos) %1 + %2")
                                  .arg(uhomeName.isEmpty() ? ".repo" : uhomeName)
                                  .arg(msys2Name.isEmpty() ? "(none)" : msys2Name) << R"(nyagos.exe)");
            }
            else
            {
                proc.setProgram("cmd.exe");
                proc.setArguments(QStringList() << "/c" << "start" << "nyagos.exe");
            }
            proc.setWorkingDirectory(repoDir);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                //QMessageBox::information(this, "確認", "コマンドプロンプトを起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "コマンドプロンプトの起動が失敗しました");
            }
        }
    });
}

void Widget::open_nyagos(QString repo)
{
    auto uhomeName = this->selectedRepoName();
    auto msys2Name = this->selectedMsys2Name();
    QString repoDir = m_env["docs"] + "/.repo/" + repo;
    this->run_process(true, "", [this, uhomeName, msys2Name, repoDir](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            //proc.setProgram(ProgramDB().wt().absoluteFilePath());
            proc.setProgram(m_env["wt"]);
            proc.setArguments(QStringList() << R"(nt)" << "--title" << QString("(Nyagos) %1 + %2")
                              .arg(uhomeName.isEmpty() ? ".repo" : uhomeName)
                              .arg(msys2Name.isEmpty() ? "(none)" : msys2Name) << R"(nyagos.exe)");
            proc.setWorkingDirectory(repoDir);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                //QMessageBox::information(this, "確認", "nyagosを起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "nyagosの起動が失敗しました");
            }
        }
    });
}

void Widget::open_bash(QString repo)
{
    auto uhomeName = this->selectedRepoName();
    auto msys2Name = this->selectedMsys2Name();
    QString repoDir = m_env["docs"] + "/.repo/" + repo;
    this->run_process(true, "", [this, uhomeName, msys2Name, repoDir](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            //proc.setProgram(ProgramDB().wt().absoluteFilePath());
            proc.setProgram(m_env["wt"]);
            proc.setArguments(QStringList() << "nt" << "--title" << QString("(BusyBox) %1 + %2")
                              .arg(uhomeName.isEmpty() ? ".repo" : uhomeName)
                              .arg(msys2Name.isEmpty() ? "(none)" : msys2Name) << "busybox.exe" << "bash" << "-l" << "-c" << QString("cd %1 && bash").arg(repoDir));
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                //QMessageBox::information(this, "確認", "bashを起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "bashの起動が失敗しました");
            }
        }
    });
}

void Widget::open_elvish(QString repo)
{
    auto uhomeName = this->selectedRepoName();
    auto msys2Name = this->selectedMsys2Name();
    QString repoDir = m_env["docs"] + "/.repo/" + repo;
    this->run_process(true, "", [this, uhomeName, msys2Name, repoDir](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            //proc.setProgram(ProgramDB().wt().absoluteFilePath());
            proc.setProgram(m_env["wt"]);
            proc.setArguments(QStringList() << "nt" << "--title" << QString("(Elvish) %1 + %2")
                              .arg(uhomeName.isEmpty() ? ".repo" : uhomeName)
                              .arg(msys2Name.isEmpty() ? "(none)" : msys2Name) << "busybox.exe" << "bash" << "-c" << "elvish.exe");
            proc.setWorkingDirectory(repoDir);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                //QMessageBox::information(this, "確認", "elvishを起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "elvishの起動が失敗しました");
            }
        }
    });
}

void Widget::open_msys2(QString repo, QString forcedMsys2)
{
    auto uhomeName = this->selectedRepoName();
    auto msys2Name = forcedMsys2.isEmpty() ? this->selectedMsys2Name() : forcedMsys2;
    QString repoDir = m_env["docs"] + "/.repo/" + repo;
    this->run_process(true, forcedMsys2, [this, uhomeName, msys2Name, repoDir](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            //proc.setProgram(ProgramDB().wt().absoluteFilePath());
            proc.setProgram(m_env["wt"]);
            proc.setArguments(QStringList() << "nt" << "--title" << QString("(Msys2) %1 + %2")
                              .arg(uhomeName.isEmpty() ? ".repo" : uhomeName)
                              .arg(msys2Name.isEmpty() ? "(none)" : msys2Name) << "cmd.exe" << "/c" << "mingw.cmd");
            proc.setWorkingDirectory(repoDir);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                //QMessageBox::information(this, "確認", "msys2を起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "msys2の起動が失敗しました");
            }
        }
    });
}

void Widget::open_git_gui(QString repo)
{
    auto uhomeName = this->selectedRepoName();
    auto msys2Name = this->selectedMsys2Name();
    QString repoDir = m_env["docs"] + "/.repo/" + repo;
    this->run_process(false, "", [this, uhomeName, msys2Name, repoDir](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            proc.setProgram("git-gui.exe");
            proc.setWorkingDirectory(repoDir);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                //QMessageBox::information(this, "確認", "git-guiを起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "git-guiの起動が失敗しました");
            }
        }
    });
}

void Widget::open_smartgit(QString repo)
{
    auto uhomeName = this->selectedRepoName();
    auto msys2Name = this->selectedMsys2Name();
    QString repoDir = m_env["docs"] + "/.repo/" + repo;
    this->run_process(false, "", [this, uhomeName, msys2Name, repoDir](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            proc.setProgram("smartgit.exe");
            proc.setArguments(QStringList() << "--open" << repoDir);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                //QMessageBox::information(this, "確認", "smartgitを起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "smartgitの起動が失敗しました");
            }
        }
    });
}

void Widget::open_git_page(QString repo)
{
    QString repoDir = m_env["docs"] + "/.repo/" + repo;
    QProcess proc;
    proc.setProgram("git.exe");
    proc.setArguments(QStringList() << "config" << "--get" << "remote.origin.url");
    proc.setWorkingDirectory(repoDir);
    proc.start();
    proc.waitForFinished();
    QDesktopServices::openUrl(QUrl(QString::fromLatin1(proc.readAll().trimmed())));
}

void Widget::open_explorer(QString repo)
{
    auto uhomeName = this->selectedRepoName();
    auto msys2Name = this->selectedMsys2Name();
    QString repoDir = m_env["docs"] + "/.repo/" + repo;
    this->run_process(false, "", [this, uhomeName, msys2Name, repoDir](QProcess &proc, ProcStage stage, bool success)
    {
        if(stage == ProcStage::SETUP)
        {
            proc.setProgram("explorer.exe");
            proc.setArguments(QStringList() << np(repoDir));
            proc.setWorkingDirectory(repoDir);
        }
        else if(stage == ProcStage::FINISH)
        {
            if(success)
            {
                //QMessageBox::information(this, "確認", "explorerを起動しました");
            }
            else
            {
                QMessageBox::information(this, "確認", "explorerの起動が失敗しました");
            }
        }
    });
}

void Widget::showContextMenuForListWidget1(const QPoint &pos)
{
    const QPoint localPos = ui->listWidget->mapFrom(this, pos);
    QModelIndex index = ui->listWidget->indexAt(localPos);
    int row = index.row();
    qDebug() << "row=" << index.row();
    if(index.row() < 0) return;
    QListWidgetItem *item = ui->listWidget->item(row);
    QString repo = item->text();
    QString repoDir = m_env["docs"] + "/.repo/" + repo;
    MySettings settings;
    QMenu contextMenu("Context menu", this);
    QAction *actSetAsHome = new QAction(QString("%1 を選択").arg(item->text()), this);
    QObject::connect(actSetAsHome, &QAction::triggered, [this, item]()
    {
        MySettings settings;
        settings.setValue(settings.SELECTED_REPO_NAME, item->text());
        this->reloadNameList(true);
    });
    if(item->text()==settings.value(settings.SELECTED_REPO_NAME))
    {
        actSetAsHome->setEnabled(false);
    }
    contextMenu.addAction(actSetAsHome);
    {
        QMenu* submenuOpenTerminal = contextMenu.addMenu(QString("%1 をターミナルで開く").arg(repo));
        QAction *actOpenNyagos = new QAction(QString("%1 をnyagosで開く").arg(repo), this);
        QObject::connect(actOpenNyagos, &QAction::triggered, [this, repo]()
        {
            this->open_nyagos(repo);
        });
        submenuOpenTerminal->addAction(actOpenNyagos);
        QAction *actOpenMsys2 = new QAction(QString("%1 をmsys2で開く").arg(repo), this);
        QObject::connect(actOpenMsys2, &QAction::triggered, [this, repo]()
        {
            this->open_msys2(repo);
        });
        submenuOpenTerminal->addAction(actOpenMsys2);
        QAction *actOpenBash = new QAction(QString("%1 をbashで開く").arg(repo), this);
        QObject::connect(actOpenBash, &QAction::triggered, [this, repo]()
        {
            this->open_bash(repo);
        });
        submenuOpenTerminal->addAction(actOpenBash);
        QAction *actOpenElvish = new QAction(QString("%1 をelvishで開く").arg(repo), this);
        QObject::connect(actOpenElvish, &QAction::triggered, [this, repo]()
        {
            this->open_elvish(repo);
        });
        submenuOpenTerminal->addAction(actOpenElvish);
    }
    {
        QMenu* submenuOpenEditor = contextMenu.addMenu(QString("%1 をエディタで開く").arg(repo));
        QAction *actOpenEmacs = new QAction(QString("%1 をemacsで開く").arg(repo), this);
        QObject::connect(actOpenEmacs, &QAction::triggered, [this, repoDir]()
        {
            this->open_emacs(repoDir);
        });
        submenuOpenEditor->addAction(actOpenEmacs);
        QAction *actOpenVscode = new QAction(QString("%1 をvscodeで開く").arg(repo), this);
        QObject::connect(actOpenVscode, &QAction::triggered, [this, repoDir]()
        {
            this->open_vscode(repoDir);
        });
        submenuOpenEditor->addAction(actOpenVscode);
    }
    if(QDir(repoDir + "/.git").exists())
    {
        QAction *actOpenGitPage = new QAction(QString("%1 のウェブサイトを開く").arg(item->text()), this);
        QObject::connect(actOpenGitPage, &QAction::triggered, [this, item]()
        {
            this->open_git_page(item->text());
        });
        contextMenu.addAction(actOpenGitPage);
        QString shims = m_env["prof"] + "/scoop/shims"; //"smartgit.exe"
        if(QFile(shims + "/smartgit.exe").exists())
        {
            QAction *actOpenSmartGit = new QAction(QString("%1 をsmartgitで開く").arg(item->text()), this);
            QObject::connect(actOpenSmartGit, &QAction::triggered, [this, item]()
            {
                this->open_smartgit(item->text());
            });
            contextMenu.addAction(actOpenSmartGit);
        }
        else if(QFile(shims + "/git-gui.exe").exists())
        {
            QAction *actOpenGitGui = new QAction(QString("%1 をgit-guiで開く").arg(item->text()), this);
            QObject::connect(actOpenGitGui, &QAction::triggered, [this, item]()
            {
                this->open_git_gui(item->text());
            });
            contextMenu.addAction(actOpenGitGui);
        }
    }
    QAction *actRemove = new QAction(QString("%1 を削除").arg(item->text()), this);
    QObject::connect(actRemove, &QAction::triggered, [this, item]()
    {
        this->remove_repo(item->text());
    });
    contextMenu.addAction(actRemove);
    if(QDir(repoDir + "/.git").exists())
    {
        QAction *actRefresh = new QAction(QString("%1 を再取得").arg(item->text()), this);
        QObject::connect(actRefresh, &QAction::triggered, [this, item]()
        {
            this->refresh_repo(item->text());
        });
        contextMenu.addAction(actRefresh);
    }
    QAction *actExplorer = new QAction(QString("%1 をエクスプローラーで表示").arg(item->text()), this);
    QObject::connect(actExplorer, &QAction::triggered, [this, item]()
    {
        this->open_explorer(item->text());
    });
    contextMenu.addAction(actExplorer);
    ProjectChecker ck(repoDir);
    //QString proFile = ck.getQtProjectFile();
    if(ck.isQtProject())
    {
        QAction *actDevelop1 = new QAction(QString("QtCreatorで開発"), this);
        QObject::connect(actDevelop1, &QAction::triggered, [this, &ck]()
        {
            QString proFile = ck.getQtProjectFile();
            if(!proFile.isEmpty())
            {
                develop_with_qtcreator(proFile);
            }
        });
        contextMenu.addAction(actDevelop1);
    }
    if(ck.isDartProject())
    {
        QAction *actDevelop2 = new QAction(QString("Android Studioで開発"), this);
        QObject::connect(actDevelop2, &QAction::triggered, [this, repoDir]()
        {
            develop_with_android_studio(repoDir);
        });
        contextMenu.addAction(actDevelop2);
    }
    if(ck.isJavaProject())
    {
        QAction *actDevelop3 = new QAction(QString("ideaで開発"), this);
        QObject::connect(actDevelop3, &QAction::triggered, [this, repoDir]()
        {
            develop_with_idea(repoDir);
        });
        contextMenu.addAction(actDevelop3);
    }
    contextMenu.exec(this->mapToGlobal(pos));
}

void Widget::showContextMenuForListWidget2(const QPoint &pos)
{
    const QPoint localPos = ui->listWidget_2->mapFrom(this, pos);
    QModelIndex index = ui->listWidget_2->indexAt(localPos);
    int row = index.row();
    qDebug() << "row=" << index.row();
    if(index.row() < 0) return;
    QListWidgetItem *item = ui->listWidget_2->item(row);
    MySettings settings;
    QMenu contextMenu("Context menu", this);
    QAction *actSetAsHome = new QAction(QString("%1 を選択").arg(item->text()), this);
    QObject::connect(actSetAsHome, &QAction::triggered, [this, item]()
    {
        MySettings settings;
        settings.setValue(settings.SELECTED_MSYS2_NAME, item->text());
        this->reloadNameList(true);
    });
    if(item->text()==settings.value(settings.SELECTED_MSYS2_NAME))
    {
        actSetAsHome->setEnabled(false);
    }
    contextMenu.addAction(actSetAsHome);
    QAction *actRemove = new QAction(QString("%1 を削除").arg(item->text()), this);
    QObject::connect(actRemove, &QAction::triggered, [this, item]()
    {
        this->remove_msys2(item->text());
    });
    contextMenu.addAction(actRemove);
    contextMenu.exec(mapToGlobal(pos));
}

void Widget::on_pushButton_3_clicked()
{
    this->install_or_update_flutter();
}

QString enclosePath(QString x)
{
    return "\"" + x + "\"";
}

// install msys2
void Widget::on_pushButton_4_clicked()
{
    this->install_msys2();
}

void Widget::on_pushButton_5_clicked()
{
    auto repo = this->selectedRepoName();
    if(repo.isEmpty())
    {
        QMessageBox::information(this, "確認", "ホームリポジトリを選択してください");
        return;
    }
    //QString uhome = m_env["docs"] + "/.repo/" + repo;
    EnvVarDialog dlg(m_env, repo);
    dlg.exec();
}


void Widget::on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
}


void Widget::on_listWidget_2_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
}


void Widget::on_pushButton_6_clicked()
{
    this->clone_repo();
}


void Widget::on_pushButton_reset_selection_clicked()
{
    m_reload_timer.stop();
    MySettings settings;
    settings.clear();
    settings.sync();
    this->reloadNameList(true);
    m_reload_timer.start();
}


void Widget::on_pushButton_nyagos_wt_clicked()
{
    auto uhomeName = this->selectedRepoName();
    this->open_nyagos(uhomeName);
}

void Widget::on_pushButton_busybox_wt_clicked()
{
    auto uhomeName = this->selectedRepoName();
    this->open_bash(uhomeName);
}


void Widget::on_pushButton_nyagos_tabby_clicked()
{
}


void Widget::on_pushButton_busybox_tabby_clicked()
{
}

void Widget::on_pushButton_test1_clicked()
{
    QString sqlitestudio = m_env["sqlitestudio"];
    QProcess proc;
    //QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    proc.setProgram(sqlitestudio);
    //proc.setProcessEnvironment(env);
    proc.setWorkingDirectory(np(m_env["docs"]));
    qDebug() << proc.startDetached();
}

void Widget::on_pushButton_test2_clicked()
{
    QString cmd1 = m_env["dir"] + "/cmd";
    QString rapidee = m_env["rapidee"]; //cmd1 + "/rapidee.exe";
    QProcess proc;
    //QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    proc.setProgram(rapidee);
    //proc.setProcessEnvironment(env);
    proc.setWorkingDirectory(np(m_env["docs"]));
    qDebug() << proc.startDetached();
}

void Widget::on_btnFolderSize_clicked()
{
    QString foldersize = m_env["foldersize"];
    QProcess proc;
    //QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    proc.setProgram(foldersize);
    //proc.setProcessEnvironment(env);
    proc.setWorkingDirectory(np(m_env["docs"]));
    qDebug() << proc.startDetached();
}


void Widget::on_btnScoopSoftware_clicked()
{
    ScoopSoftwareDialog dlg(m_env);
    if(dlg.exec())
    {
        QStringList nameList = dlg.nameList();
        QMap<QString, QVariantMap> statusMap = dlg.statusMap();
        QSet<QString> bucketSet;
        QStringList installList;
        QStringList uninstallList;
        foreach(QString name, nameList)
        {
            bool currentlyInstalled = (QDir(m_env["prof"] + "/scoop/apps/" + name).exists());
            bool toBeInstalled = statusMap[name]["installed"].toBool();
            if(currentlyInstalled == toBeInstalled) continue;
            if(currentlyInstalled)
            {
                uninstallList.append(name);
            }
            else
            {
                installList.append(name);
                bucketSet.insert(statusMap[name]["bucket"].toString());
            }
        }
        if(installList.size()==0 && uninstallList.size()==0)
        {
            QMessageBox::information(this, "確認", "何もすることがありません");
            return;
        }
        qDebug() << "bucketSet" << bucketSet;
        qDebug() << "installList" << installList;
        qDebug() << "uninstallList" << uninstallList;
        QString buffer;
        QTextStream strm(&buffer);
        QStringList bucketList = bucketSet.values();
        strm << QString("#! bash -uvx") << Qt::endl;
        foreach(QString bucket, bucketList)
        {
            strm << QString("scoop bucket add %1").arg(bucket) << Qt::endl;
        }
        if(installList.size() > 0)
        {
            strm << QString("scoop install %1").arg(installList.join(" ")) << Qt::endl;
        }
        if(uninstallList.size() > 0)
        {
            strm << QString("scoop uninstall %1").arg(uninstallList.join(" ")) << Qt::endl;
        }
        strm << Qt::flush;
        QString cmdLines = *strm.string();
        CmdProcess *proc = new CmdProcess(m_env, QString("scoopパッケージインストール/アンインストール"), cmdLines, ".sh");
        proc->run();
    }
}


void Widget::on_btnElvish_clicked()
{
    auto uhomeName = this->selectedRepoName();
    this->open_elvish(uhomeName);
}


void Widget::on_btnMsys2_clicked()
{
    auto uhomeName = this->selectedRepoName();
    this->open_msys2(uhomeName);
}


void Widget::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    this->open_nyagos(item->text());
    //this->open_cmd(item->text());
}


void Widget::on_listWidget_2_itemDoubleClicked(QListWidgetItem *item)
{
    auto uhomeName = this->selectedRepoName();
    this->open_msys2(uhomeName, item->text());
}

void Widget::on_btnChrome_clicked()
{
    QFileInfo chrome = ProgramDB().chrome();
    if(!chrome.exists())
    {
        QMessageBox::information(this, "確認", "googlechromeがインストールされていません");
    }
    QProcess proc;
    proc.setProgram(chrome.absoluteFilePath());
    proc.setWorkingDirectory(np(m_env["docs"]));
    qDebug() << proc.startDetached();
}


void Widget::on_btnOpenSettingsFolder_clicked()
{
    QProcess proc;
    proc.setProgram("explorer.exe");
    proc.setArguments(QStringList() << np(m_env["prof"] + "/AppData/Roaming/javacommons"));
    proc.setWorkingDirectory(np(m_env["docs"]));
    qDebug() << proc.startDetached();
}

