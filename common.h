#ifndef COMMON_H
#define COMMON_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include "binarysettings.h"

class MySettings: public BinarySettings
{
public:
    MySettings(): BinarySettings(JsonFormat, QSettings::UserScope,
                                     "javacommons", "spider")
    {
    }
    const QString SELECTED_REPO_NAME = "selected/repoName";
    const QString SELECTED_MSYS2_NAME = "selected/msys2Name";
};

class SpiderSettings
{
public:
    SpiderSettings(QMap<QString, QString> env, QString repo)
    {
        QString repoDir = env["docs"] + "/.repo";
        QString fileName = repoDir;
        if(!repo.isEmpty())
        {
            fileName += "/" + repo;
        }
        fileName += "/.spider.json";
        qDebug() << "SpiderSettings" << fileName;
        m_settings = new BinarySettings(fileName, JsonFormat);
    }
    virtual ~SpiderSettings()
    {
        delete m_settings;
    }
    QSettings &settings()
    {
        return *(this->m_settings);
    }
private:
    BinarySettings *m_settings;
};

static inline QString np(QString x)
{
    return x.replace("/", "\\");
}

static inline void updateListWidgetContent(QListWidget *lw, QMap<QString, bool> *newContent, const QString &selectedName, bool force=false)
{
    QMap<QString, bool> currRepoList;
    QString currentName;
    for(int i=0; i<lw->count(); i++)
    {
        if(lw->item(i)->isSelected()) currentName = lw->item(i)->text();
        if(lw->item(i)->foreground() == Qt::red)
        {
            currRepoList.insert(lw->item(i)->text(), true);
        }
        else
        {
            currRepoList.insert(lw->item(i)->text(), false);
        }
    }
    if(!force && currRepoList == *newContent) return;
    lw->blockSignals(true);
    lw->clear();
    lw->blockSignals(false);
    QStringList newContentList = newContent->keys();
    newContentList.sort();
    for(int i=0; i<newContentList.count(); i++)
    {
        QString dir = newContentList.at(i);
        qDebug() << dir;
        lw->addItem(dir);
        if(dir==currentName)
        {
            lw->item(lw->count()-1)->setSelected(true);
        }
        if(dir==selectedName)
        {
            lw->item(lw->count()-1)->setIcon(QApplication::style()->standardPixmap(QStyle::SP_ArrowForward));
        }
        else
        {
            lw->item(lw->count()-1)->setIcon(QApplication::style()->standardPixmap(QStyle::SP_DirIcon));
        }
        if((*newContent)[dir])
        {
            lw->item(lw->count()-1)->setForeground(Qt::red);
        }
    }
}

#endif // COMMON_H
