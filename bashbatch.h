#ifndef BASHBATCH_H
#define BASHBATCH_H

#include <QtCore>

class BashBatch//: public QObject
{
    //Q_OBJECT
public:
    BashBatch(QMap<QString, QString> &env);
    int exec(QString cmd, QString workingDirectory);

private:
    QMap<QString, QString> m_env;
};

#endif // BASHBATCH_H
