#ifndef PROGRAMDB_H
#define PROGRAMDB_H

#include <QtCore>

class ProgramDB
{
public:
    ProgramDB();
    //QFileInfo wt() const;
    QFileInfo android_studio() const;
    QFileInfo idea() const;
    QFileInfo emacs() const;
    QFileInfo vscode() const;
    QFileInfo chrome() const;

private:
    //QFileInfo m_wt;
    QFileInfo m_android_studio;
    QFileInfo m_idea;
    QFileInfo m_emacs;
    QFileInfo m_vscode;
    QFileInfo m_chrome;
};

#endif // PROGRAMDB_H
