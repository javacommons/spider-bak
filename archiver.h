#ifndef ARCHIVER_H
#define ARCHIVER_H

#include <QtCore>
#include <archive.h>
#include <archive_entry.h>
#include <sys/utime.h>

bool extract_archive(const QString &archive_path, const QString &output_path);

#endif // ARCHIVER_H
