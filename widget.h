#ifndef WIDGET_H
#define WIDGET_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include <functional>
#include <utility>

QT_BEGIN_NAMESPACE
namespace Ui
{
class Widget;
}
QT_END_NAMESPACE

enum ProcStage
{
    SETUP,
    FINISH
};

using ProcCallback = std::function<void(QProcess &proc, ProcStage stage, bool success)>;

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(const QMap<QString, QString> env, QWidget *parent = nullptr);
    ~Widget();

private:
    QMap<QString, QString> m_env;
    QTimer m_reload_timer;

    void reloadNameList(bool force = false);
    QString selectedRepoName();
    QString selectedMsys2Name();
    void run_process(bool need_msys2, QString forcedMsys2, ProcCallback callback);
    void clone_repo();
    void remove_repo(QString repo);
    void refresh_repo(QString repo);
    void develop_with_qtcreator(QString proFile);
    void develop_with_android_studio(QString repoDir);
    void develop_with_idea(QString repoDir);
    void open_emacs(QString repoDir);
    void open_vscode(QString repoDir);
    void install_or_update_flutter();
    void install_msys2();
    void remove_msys2(QString name);
    void open_cmd(QString repo);
    void open_nyagos(QString repo);
    void open_bash(QString repo);
    void open_elvish(QString repo);
    void open_msys2(QString repo, QString forcedMsys2 = "");
    void open_git_gui(QString repo);
    void open_smartgit(QString repo);
    void open_git_page(QString repo);
    void open_explorer(QString repo);

private slots:
    void showContextMenuForListWidget1(const QPoint &);

    void showContextMenuForListWidget2(const QPoint &);

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_listWidget_2_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_pushButton_6_clicked();

    void on_pushButton_reset_selection_clicked();

    void on_pushButton_nyagos_wt_clicked();

    void on_pushButton_busybox_wt_clicked();

    void on_pushButton_nyagos_tabby_clicked();

    void on_pushButton_busybox_tabby_clicked();

    void on_pushButton_test1_clicked();

    void on_pushButton_test2_clicked();

    void on_btnFolderSize_clicked();

    void on_btnScoopSoftware_clicked();

    void on_btnElvish_clicked();

    void on_btnMsys2_clicked();

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_listWidget_2_itemDoubleClicked(QListWidgetItem *item);

    void on_btnChrome_clicked();

    void on_btnOpenSettingsFolder_clicked();

private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
