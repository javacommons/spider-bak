#include "qsettings_binary.h"
#include <QtCore>

#include "variantserializer.h"

#if 0x0
static QString variantToString(const QVariant &v)
{
    QJsonDocument jdoc = QJsonDocument::fromVariant(v);
    QString json = QString::fromUtf8(jdoc.toJson(QJsonDocument::Compact));
    if(!json.isEmpty())
    {
        return json;
    }
    QVariantList vl = { v };
    jdoc = QJsonDocument::fromVariant(vl);
    json = QString::fromUtf8(jdoc.toJson(QJsonDocument::Compact));
    json = json.mid(1, json.length()-2); // remove [ & ]
    return json;
}
#endif

bool readSettingsBinary(QIODevice &device, QMap<QString, QVariant> &map)
{
    QByteArray json = device.readAll();
    QJsonParseError error;
    QJsonDocument jdoc = QJsonDocument::fromJson(json, &error);
    if(error.error != QJsonParseError::NoError)
    {
        return false;
    }
    QJsonObject obj = jdoc.object();
    QStringList keys = obj.keys();
    map.clear();
    foreach(QString key, keys)
    {
        if(obj[key].isObject())
        {
            QJsonObject obj2 = obj[key].toObject();
            QStringList keys2 = obj2.keys();
            foreach(QString key2, keys2)
            {
                //QString bin2 = obj2[key2].toString();
                //bin2 = bin2.split(" ").at(0);
                QVariant v2 = VariantSerializer().deserializeFromString(obj2[key2].toString());
                map[key+"/"+key2] = v2;
            }
            continue;
        }
        //QString bin = obj[key].toString();
        //bin = bin.split(" ").at(0);
        QVariant v = VariantSerializer().deserializeFromString(obj[key].toString());
        map[key] = v;
    }
    return true;
}

bool writeSettingsBinary(QIODevice &device, const QMap<QString, QVariant> &map)
{
    QVariantMap map2;
    QStringList keys = map.keys();
    foreach(QString key, keys)
    {
        //QString bin = VariantSerializer().serialize(map[key]);
        //bin += " " + variantToString(map[key]);
        QString bin = VariantSerializer().serializeToString(map[key]);
        if(key.contains("/"))
        {
            QStringList groupKeys = key.split("/");
            if(groupKeys.size() == 2)
            {
                QVariantMap groupMap = map2[groupKeys[0]].toMap();
                groupMap[groupKeys[1]] = bin;
                map2[groupKeys[0]] = groupMap;
            }
        }
        else
        {
            map2[key] = bin;
        }
    }
    QJsonDocument jdoc = QJsonDocument::fromVariant(map2);
    device.write(jdoc.toJson());
    return true;
}
