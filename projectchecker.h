#ifndef PROJECTCHECKER_H
#define PROJECTCHECKER_H

#include <QtCore>

class ProjectChecker
{
public:
    ProjectChecker(QString dir);
    bool isHome();
    QString getQtProjectFile();
    bool isGitDir();
    bool isQtProject();
    bool isDartProject();
    bool isJavaProject();
    bool isVisible();

private:
    QString m_dir;
};

#endif // PROJECTCHECKER_H
