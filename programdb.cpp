#include "programdb.h"

static void copyPath(QString src, QString dst) // https://stackoverflow.com/questions/2536524/copy-directory-using-qt
{
    QDir dir(src);
    if (! dir.exists())
        return;
    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot))
    {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyPath(src+ QDir::separator() + d, dst_path);
    }
    foreach (QString f, dir.entryList(QDir::Files))
    {
        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
    }
}

ProgramDB::ProgramDB()
{
    QString prof = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    QString scoop = prof + "/scoop";
    QString wt_src_dir = qApp->applicationDirPath() + "/wt-1.12.10393.0";
#if 0x0
    QString wt_dst_dir = prof + "/.software/WindowsTerminal/wt-1.12.10393.0";
    if(!QDir(wt_dst_dir).exists())
    {
        copyPath(wt_src_dir, wt_dst_dir);
    }
    m_wt = wt_dst_dir + "/wt.exe";
#endif
    m_android_studio = prof + "/scoop/apps/android-studio/current/bin/studio64.exe";
    m_idea = prof + "/scoop/apps/idea/current/IDE/bin/idea64.exe";
    m_emacs = prof + "/scoop/apps/emacs/current/bin/emacs.exe";
    m_vscode = prof + "/scoop/apps/vscode/current/Code.exe";
    m_chrome = prof + "/scoop/apps/googlechrome/current/chrome.exe";
}

#if 0x0
QFileInfo ProgramDB::wt() const
{
    return m_wt;
}
#endif

QFileInfo ProgramDB::android_studio() const
{
    return m_android_studio;
}

QFileInfo ProgramDB::idea() const
{
    return m_idea;
}

QFileInfo ProgramDB::emacs() const
{
    return m_emacs;
}

QFileInfo ProgramDB::vscode() const
{
    return m_vscode;
}

QFileInfo ProgramDB::chrome() const
{
    QString system_chrome = "C:/Program Files/Google/Chrome/Application/chrome.exe";
    if(QFileInfo(system_chrome).exists())
    {
        return system_chrome;
    }
    return m_chrome;
}
